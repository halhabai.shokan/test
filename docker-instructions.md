#Build  docker image:
docker build -t my-nginx-image .

#Run the container using image created previously:
docker run -d -p 80:80 -name my-nginx-container my-nginx-image

#Check the running container:
docker ps

#Displaying content:
open https://localhost
