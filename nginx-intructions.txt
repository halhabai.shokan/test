#Установка и настройка Nginx:

#Установка:
sudo apt update
sudo apt install nginx
sudo systemctl start nginx
sudo systemctl enable nginx

#Настройка:

#Копировать код и вставить в терминал(Код создает  html файл и хранит его по указанному адресу)
sudo mkdir -p /var/www/html
echo '<!DOCTYPE html>
<html>
<head>
    <title>Welcome to Nginx</title>
</head>
<body>
    <h1>Hello, DevOps World!</h1>
</body>
</html>' | sudo tee /var/www/html/index.html

#Убедитесь что в файле /etc/nginx/sites-available/default путь указывает на var/www/html
nginx server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;

    server_name _;

    location / {
        try_files $uri $uri/ =404;
    }
}

#Проверьте конфигурацию и перезагрузите:
sudo nginx -t
sudo systemctl reload nginx


